## Projeto de Engenharia de Software II - 2016.1 - UFS

# PetSmart

O PetSmart será um sistema de gerenciamento personalizado para clínicas veterinárias. Terá como principais características o gerenciamento de clientes e animais, funcionários, venda de produtos e de assistência veterinária.

## Ambiente de projeto

 * Eclipse Neon EE
 * Tomcat 9
 * MySQL (base sem senha)
 * JDK 8
 
 
