package br.com.esii.petsmart.agenda;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import br.com.esii.petsmart.animal.Animal;
import br.com.esii.petsmart.cliente.Cliente;
import br.com.esii.petsmart.consulta.Consulta;
import br.com.esii.petsmart.exame.Exame;
import br.com.esii.petsmart.veterinario.Veterinario;

@Entity
@Table(name = "agenda")
public class Agenda implements Serializable {

	private static final long serialVersionUID = 5737999401643037693L;

	@EmbeddedId
    protected AgendaPK agendaPK;
    
    @Lob
    @Size(max = 65535)
    @Column(name = "obs")
    private String obs;
    
    @Lob
    @Size(max = 65535)
    @Column(name = "resultado")
    private String resultado;
    
    @JoinColumn(name = "cliente_id", referencedColumnName = "cliente_id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Cliente cliente;
    
    @JoinColumn(name = "animal_id", referencedColumnName = "animal_id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Animal animal;
    
    @JoinColumn(name = "exame_id", referencedColumnName = "exame_id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Exame exame;
    
    @JoinColumn(name = "consulta_id", referencedColumnName = "consulta_id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Consulta consulta;
    
    @JoinColumn(name = "veterinario_id", referencedColumnName = "veterinario_id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Veterinario veterinario;

    public Agenda() {
    }

    public Agenda(AgendaPK agendaPK) {
        this.agendaPK = agendaPK;
    }

    public Agenda(Date dataHora, int veterinarioId, int ExameId,int consultaId , int clienteId, int animalId) {
        this.agendaPK = new AgendaPK(dataHora, veterinarioId, ExameId, consultaId , clienteId, animalId);
    }

    public AgendaPK getAgendaPK() {
        return agendaPK;
    }

    public void setAgendaPK(AgendaPK agendaPK) {
        this.agendaPK = agendaPK;
    }

    public String getObs() {
        return obs;
    }

    public void setObs(String obs) {
        this.obs = obs;
    }

    public String getResultado() {
        return resultado;
    }

    public void setResultado(String resultado) {
        this.resultado = resultado;
    }

    public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public Veterinario getVeterinario() {
		return veterinario;
	}

	public void setVeterinario(Veterinario veterinario) {
		this.veterinario = veterinario;
	}

	public Exame getExame() {
        return exame;
    }

    public void setExame(Exame exame) {
        this.exame = exame;
    }


    @Override
    public int hashCode() {
        int hash = 0;
        hash += (agendaPK != null ? agendaPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Agenda)) {
            return false;
        }
        Agenda other = (Agenda) object;
        if ((this.agendaPK == null && other.agendaPK != null) || (this.agendaPK != null && !this.agendaPK.equals(other.agendaPK))) {
            return false;
        }
        return true;
    }

}
