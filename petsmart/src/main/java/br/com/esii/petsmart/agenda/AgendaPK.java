package br.com.esii.petsmart.agenda;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

@Embeddable
public class AgendaPK implements Serializable {
    
	private static final long serialVersionUID = -3590815525309366754L;
	
	@Basic(optional = false)
    @NotNull
    @Column(name = "dataHora")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dataHora;
    @Basic(optional = false)
    @NotNull
    @Column(name = "veterinario_id")
    private int veterinarioId;
    @Basic(optional = false)
    @NotNull
    @Column(name = "exame_id")
    private int ExameId;
    @Basic(optional = false)
    @NotNull
    @Column(name = "consulta_id")
    private int consultaId;
    @Basic(optional = false)
    @NotNull
    @Column(name = "cliente_id")
    private int clienteId;
    @NotNull
    @Column(name = "animal_id")
    private int animalId;

    public AgendaPK() {
    }

    public AgendaPK(Date dataHora, int veterinarioId, int ExameId,int consultaId , int clienteId, int animalId) {
        this.dataHora = dataHora;
        this.veterinarioId = veterinarioId;
        this.ExameId = ExameId;
        this.consultaId = consultaId;
        this.clienteId = clienteId;
        this.animalId = animalId;
    }

    public Date getDataHora() {
        return dataHora;
    }

    public void setDataHora(Date dataHora) {
        this.dataHora = dataHora;
    }

	public int getVeterinarioId() {
		return veterinarioId;
	}

	public void setVeterinarioId(int veterinarioId) {
		this.veterinarioId = veterinarioId;
	}

	public int getExameId() {
		return ExameId;
	}

	public void setExameId(int exameId) {
		ExameId = exameId;
	}

	public int getClienteId() {
		return clienteId;
	}

	public void setClienteId(int clienteId) {
		this.clienteId = clienteId;
	}

	public int getAnimalId() {
		return animalId;
	}

	public void setAnimalId(int animalId) {
		this.animalId = animalId;
	}

	public int getConsultaId() {
		return consultaId;
	}

	public void setConsultaId(int consultaId) {
		this.consultaId = consultaId;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ExameId;
		result = prime * result + animalId;
		result = prime * result + clienteId;
		result = prime * result + consultaId;
		result = prime * result + ((dataHora == null) ? 0 : dataHora.hashCode());
		result = prime * result + veterinarioId;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AgendaPK other = (AgendaPK) obj;
		if (ExameId != other.ExameId)
			return false;
		if (animalId != other.animalId)
			return false;
		if (clienteId != other.clienteId)
			return false;
		if (consultaId != other.consultaId)
			return false;
		if (dataHora == null) {
			if (other.dataHora != null)
				return false;
		} else if (!dataHora.equals(other.dataHora))
			return false;
		if (veterinarioId != other.veterinarioId)
			return false;
		return true;
	}
	
}
