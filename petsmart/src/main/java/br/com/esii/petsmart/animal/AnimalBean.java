package br.com.esii.petsmart.animal;


import java.util.ArrayList;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;

import org.primefaces.context.RequestContext;

import br.com.esii.petsmart.cliente.Cliente;
import br.com.esii.petsmart.cliente.ClienteRN;
import br.com.esii.petsmart.raca.Raca;

@SessionScoped
@ManagedBean(name = "animalBean")
public class AnimalBean {

	private Animal animal;
	private Animal animalSelecionado;
    private Cliente cliente;
	public AnimalBean() {
		animal = new Animal();
	}

	public Animal getAnimal() {	 
		if(animal == null){
		animal = new Animal();
		}
		return animal;
	}
	
	public void setAnimal(Animal animal) {
		this.animal = animal;
	}


	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	
	

	public Animal getAnimalSelecionado() {
		return animalSelecionado;
	}

	public void setAnimalSelecionado(Animal animalSelecionado) {
		this.animalSelecionado = animalSelecionado;
	}
	
	
   /*Salvar animal*/
	public void salvar() {
		AnimalRN animalRN = new AnimalRN();
		animalRN.salvar(animal);
		Animal animal = new Animal();
		this.animal = animal;
	}

	/* Verifica se o cliente tem mais de um animal e salva o animal novo*/
	public void associarPet(){
		
		System.out.println("Nome do cliente -" + cliente.getNome());

		AnimalRN animalRN = new AnimalRN();
		ClienteRN clienteRN = new ClienteRN();

		List<Animal> pets;

		try {

			if (!cliente.getPets().isEmpty()) {
				System.out.println("---------------> 1");
				pets = cliente.getPets();
				animalRN.salvar(animal);
				pets.add(animal);
			} else {
				System.out.println("---------------> 2");
				pets = new ArrayList<>();
				animal.setDono(cliente);
				animalRN.salvar(animal);
				pets.add(animal);
			}

			cliente.setPets(pets);
			
			clienteRN.atualizar(cliente);
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "", "Pet criado com sucesso."));
			RequestContext.getCurrentInstance().execute("CadastroPet.hide();");
			RequestContext.getCurrentInstance().execute("$('#createButton').fadeOut();");
			animal = new Animal();
		} catch (Exception e) {
			FacesContext.getCurrentInstance().addMessage("CadastroPetform",new FacesMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), null));
		}

	}
	
	/*alterar as informações do animal de um cliente*/
	public void alterarPetAssociado(){
		AnimalRN animalRN = new AnimalRN();
		
		try{
			
	    animalRN.atualizar(animalSelecionado);
	    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "", "Pet alterado com sucesso."));
		RequestContext.getCurrentInstance().execute("EditPet.hide();");
		RequestContext.getCurrentInstance().execute("$('#editButton').fadeOut();");	
		} catch (Exception e) {
			FacesContext.getCurrentInstance().addMessage("EditPetform",new FacesMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), null));
		}
		
	}

	/*Exclui um animal selecionado da lista de clientes*/
	public void excluirPetAssociado(){
      AnimalRN animalRN = new AnimalRN();
		
		try{
	    animalRN.excluir(animalSelecionado);
	    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "", "Pet deletado com sucesso."));	
		} catch (Exception e) {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "", "Erro ao deletar o pet."));
		}
	}
	
}
