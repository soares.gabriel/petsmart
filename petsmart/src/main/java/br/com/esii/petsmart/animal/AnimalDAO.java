package br.com.esii.petsmart.animal;

import java.util.List;

public interface AnimalDAO {

	public void salvar(Animal animal);
	public void atualizar(Animal animal);
	public void excluir(Animal animal);
	public Animal carregar(Integer id);
	public List<Animal> listar();
	
}
