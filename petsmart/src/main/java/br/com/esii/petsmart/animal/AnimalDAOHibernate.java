package br.com.esii.petsmart.animal;

import java.util.List;
import org.hibernate.Session;

public class AnimalDAOHibernate implements AnimalDAO {
	
private Session session;
	
	public void setSession(Session session){
		this.session = session;
	}

	@Override
	public void salvar(Animal animal) {
		this.session.save(animal);
	}

	@Override
	public void atualizar(Animal animal) {
		this.session.update(animal);
	}

	@Override
	public void excluir(Animal animal) {
		this.session.delete(animal);
	}

	@Override
	public Animal carregar(Integer id) {		
		return (Animal) this.session.get(Animal.class, id);
	}

	@SuppressWarnings({"unchecked"})
	@Override
	public List<Animal> listar() {
	   return this.session.createCriteria(Animal.class).list();
	}


}
