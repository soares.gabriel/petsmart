package br.com.esii.petsmart.animal;

import java.util.List;
import br.com.esii.petsmart.util.DAOFactory;

public class AnimalRN {
	
	private AnimalDAO animalDAO;

	public AnimalRN() {
		this.animalDAO = DAOFactory.criarAnimalDAO();
	}

	public Animal carregar(Integer id) {
		return this.animalDAO.carregar(id);
	}

	public void salvar(Animal animal) {
		this.animalDAO.salvar(animal);
	}

	public void atualizar(Animal animal) {
        this.animalDAO.atualizar(animal);
	}

	public void excluir(Animal animal) {
		this.animalDAO.excluir(animal);
	}

	public List<Animal> listar() {
		return this.animalDAO.listar();
	}

}
