package br.com.esii.petsmart.cargo;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import br.com.esii.petsmart.entidades.EntidadeAbst;


@Entity
@Table(name="cargo")
public class Cargo extends EntidadeAbst implements Serializable{

	private static final long serialVersionUID = -7919766632444279944L;

	@Id
	@GeneratedValue
	@Column(name="cargo_id")
	private Integer id;
	
	@Column
	private String cargo;
	
	@Column
	private Double salario;
	
	@Column
	private Date dataReajuste;
	
	@Column
	private String tipo;
	
	public Cargo(){}
	
	

	public Integer getId() {
		return id;
	}


	public void setId(Integer id) {
		this.id = id;
	}

	public String getCargo() {
		return cargo;
	}

	public void setCargo(String cargo) {
		this.cargo = cargo;
	}

	public Double getSalario() {
		return salario;
	}

	public void setSalario(Double salario) {
		this.salario = salario;
	}

	public Date getDataReajuste() {
		return dataReajuste;
	}

	public void setDataReajuste(Date dataReajuste) {
		this.dataReajuste = dataReajuste;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((cargo == null) ? 0 : cargo.hashCode());
		result = prime * result + ((dataReajuste == null) ? 0 : dataReajuste.hashCode());
		result = prime * result + id;
		result = prime * result + ((salario == null) ? 0 : salario.hashCode());
		result = prime * result + ((tipo == null) ? 0 : tipo.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		Cargo other = (Cargo) obj;
		if (cargo == null) {
			if (other.cargo != null)
				return false;
		} else if (!cargo.equals(other.cargo))
			return false;
		if (dataReajuste == null) {
			if (other.dataReajuste != null)
				return false;
		} else if (!dataReajuste.equals(other.dataReajuste))
			return false;
		if (id != other.id)
			return false;
		if (salario == null) {
			if (other.salario != null)
				return false;
		} else if (!salario.equals(other.salario))
			return false;
		if (tipo == null) {
			if (other.tipo != null)
				return false;
		} else if (!tipo.equals(other.tipo))
			return false;
		return true;
	}
	
	
	
}
