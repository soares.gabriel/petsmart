package br.com.esii.petsmart.cargo;

import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;


@ManagedBean(name="cargoBean")
@SessionScoped
public class CargoBean {
	
	private Cargo cargo;
	private Cargo cargoSelecionado;
	
	public CargoBean(){	
		this.cargo = new Cargo();
		this.cargoSelecionado = new Cargo();
	}

	public Cargo getCargo() {
		return cargo;
	}

	public void setCargo(Cargo cargo) {
		this.cargo = cargo;
	}

	public Cargo getCargoSelecionado() {
		if(cargoSelecionado == null){
			cargoSelecionado = new Cargo();
		}
		return cargoSelecionado;
	}

	public void setCargoSelecionado(Cargo cargoSelecionado) {
		this.cargoSelecionado = cargoSelecionado;
	}

	public void salvar(){
		CargoRN cargoRN = new CargoRN();
		cargoRN.salvar(cargo);
		cargo = new Cargo();
		FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "", "Criado com sucesso."));
	}
	
	public List<Cargo> listarTodos(){
		CargoRN cargoRN = new CargoRN();
		return cargoRN.listar();
	}
	
	public void alterar(){
		CargoRN cargoRN = new CargoRN();
		cargoRN.atualizar(cargoSelecionado);
		FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "", "Alterado com sucesso."));
	}
	
	public void deletar(){
		CargoRN cargoRN = new CargoRN();
		cargoRN.excluir(cargoSelecionado);
		FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "", "Deletado com sucesso."));
	}

}
