package br.com.esii.petsmart.cargo;

import java.util.List;


public interface CargoDAO {


	public void salvar(Cargo cargo);
	public void atualizar(Cargo cargo);
	public void excluir(Cargo cargo);
	public Cargo carregar(Integer id);
	public List<Cargo> listar();

}
