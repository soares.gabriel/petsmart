
package br.com.esii.petsmart.cargo;

import java.util.List;

import org.hibernate.Session;

import br.com.esii.petsmart.raca.Raca;

public class CargoDAOHibernate implements CargoDAO {
	
	private Session session;
	
	public void setSession(Session session){
		this.session = session;
	}

	@Override
	public void salvar(Cargo cargo) {
		this.session.save(cargo);
		
	}

	@Override
	public void atualizar(Cargo cargo) {
		this.session.update(cargo);
		
	}

	@Override
	public void excluir(Cargo cargo) {
		this.session.delete(cargo);
		
	}

	@Override
	public Cargo carregar(Integer id) {
		return (Cargo) this.session.get(Cargo.class, id);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Cargo> listar() {
		return this.session.createCriteria(Cargo.class).list();
	}

}
