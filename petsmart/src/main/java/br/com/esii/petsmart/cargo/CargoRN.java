package br.com.esii.petsmart.cargo;

import java.util.List;

import br.com.esii.petsmart.util.DAOFactory;

public class CargoRN {
	
	private CargoDAO cargoDAO;

	public CargoRN() {
		this.cargoDAO = DAOFactory.criarCargoDAO();
	}

	public Cargo carregar(Integer id) {
		return this.cargoDAO.carregar(id);
	}

	public void salvar(Cargo cargo) {
		this.cargoDAO.salvar(cargo);
	}

	public void atualizar(Cargo cargo) {
         this.cargoDAO.atualizar(cargo);
	}

	public void excluir(Cargo cargo) {
		this.cargoDAO.excluir(cargo);
	}

	public List<Cargo> listar() {
		return this.cargoDAO.listar();
	}

}
