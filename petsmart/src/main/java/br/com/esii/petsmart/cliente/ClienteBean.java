package br.com.esii.petsmart.cliente;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;

import br.com.esii.petsmart.animal.Animal;
import br.com.esii.petsmart.animal.AnimalRN;
import br.com.esii.petsmart.endereco.Endereco;
import br.com.esii.petsmart.endereco.EnderecoRN;


@ManagedBean(name="clienteBean")
@SessionScoped
public class ClienteBean {
	
	private Cliente cliente;
	private Endereco endereco;
	private Animal animal;
	private Cliente clienteSelecionado;
	
	public ClienteBean(){
		cliente = new Cliente();
		clienteSelecionado = new Cliente();
		endereco = new Endereco();
		animal = new Animal();
	}
	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public Cliente getClienteSelecionado() {
		return clienteSelecionado;
	}

	public void setClienteSelecionado(Cliente clienteSelecionado) {
		this.clienteSelecionado = clienteSelecionado;
	}
	
	public Endereco getEndereco() {
		return endereco;
	}
	public void setEndereco(Endereco endereco) {
		this.endereco = endereco;
	}
	public Animal getAnimal() {
		return animal;
	}
	public void setAnimal(Animal animal) {
		this.animal = animal;
	}
	
	/*Redirecionamento para a pagina de cadastro de cliente*/
	public String novo(){
		return "/restrito/clinica/cliente/Create";
	}
	
	/*Redirecionamento para a pagina de altera��o de cliente*/
	public String editar(){
		return "/restrito/clinica/cliente/Edit";
	}
	
	/*Redirecionamento para a pagina de gerenciamento de todos os clientes */
	public String inicial(){
		return "/restrito/clinica/cliente/index";
	}
	
	/*Redirecionamento para a pagina de cadastro de animais e o gerecimento de animais*/
	public String pets(){
		return "/restrito/clinica/cliente/pets";
	}

	/*Salvar os dados do cliente e o seu endereco*/
	public String salvar(){
		ClienteRN clienteRN = new ClienteRN();
	    EnderecoRN enderecoRN = new EnderecoRN();
		enderecoRN.salvar(endereco);
		cliente.setEndereco(endereco);
		clienteRN.salvar(cliente);
		Endereco enderecoNull = new Endereco();
		this.endereco = enderecoNull;
		Cliente clienteNull = new Cliente();
		this.cliente = clienteNull;
		FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "", "Criado com sucesso."));
		return "/restrito/clinica/cliente/index";
	}
	
	/*Lista todos os clientes cadastrados no sistema*/
	public List<Cliente> listarTodos(){
		ClienteRN clienteRN = new ClienteRN();
		return clienteRN.listar();
	}
	
	
	/*Altera os dados do cliente*/
	public String alterar(){
		ClienteRN clienteRN = new ClienteRN();
		EnderecoRN enderecoRN = new EnderecoRN();
		clienteRN.atualizar(clienteSelecionado);
		enderecoRN.atualizar(clienteSelecionado.getEndereco());
		Endereco enderecoNull = new Endereco();
		this.endereco = enderecoNull;
		Cliente clienteNull = new Cliente();
		this.clienteSelecionado = clienteNull;
		FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "", "Alterado com sucesso."));
		return "/restrito/clinica/cliente/Edit";
	}
	
	/*Deleta uma cliente do sistema*/
	public void deletar(){
		ClienteRN clienteRN = new ClienteRN();
		EnderecoRN enderecoRN = new EnderecoRN();
		enderecoRN.excluir(clienteSelecionado.getEndereco());
		clienteRN.excluir(clienteSelecionado);
		FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "", "Deletado com sucesso."));
	}
	
	
	/*Altera��o no padr�o de data do componente Calender do primefaces*/
	public void onDateSelect(SelectEvent event) {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
        facesContext.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Date Selected", format.format(event.getObject())));
    }
     
	/*Chamada para abrir um dialog do primefaces*/
    public void click() {
        RequestContext requestContext = RequestContext.getCurrentInstance();
         
        requestContext.update("formCliente:display");
        requestContext.execute("PF('dlg').show()");
    }
	
}
