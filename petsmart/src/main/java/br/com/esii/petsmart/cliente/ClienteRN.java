package br.com.esii.petsmart.cliente;

import java.util.List;
import br.com.esii.petsmart.util.DAOFactory;

public class ClienteRN {
	
	private ClienteDAO clienteDAO;

	public ClienteRN() {
		this.clienteDAO = DAOFactory.criarClienteDAO();
	}

	public Cliente carregar(Integer id) {
		return this.clienteDAO.carregar(id);
	}

	public void salvar(Cliente cliente) {
		this.clienteDAO.salvar(cliente);
	}

	public void atualizar(Cliente cliente) {
         this.clienteDAO.atualizar(cliente);
	}

	public void excluir(Cliente cliente) {
		this.clienteDAO.excluir(cliente);
	}

	public List<Cliente> listar() {
		return this.clienteDAO.listar();
	}

}
