package br.com.esii.petsmart.consulta;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import javax.persistence.*;

import br.com.esii.petsmart.entidades.EntidadeAbst;
import br.com.esii.petsmart.raca.Raca;
import br.com.esii.petsmart.veterinario.Veterinario;
import br.com.esii.petsmart.exame.Exame;
import br.com.esii.petsmart.animal.Animal;


@Entity
@Table(name="consulta")
public class Consulta extends EntidadeAbst implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 7299784313424545623L;

	@GeneratedValue
	@Id
	@Column(name="consulta_id")
	private int id;
	
	@Column
	private String resumo;
	
	@Column 
	private Date data;
	
	public Date getData() {
		return data;
	}

	public void setData(Date data) {
		this.data = data;
	}

	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name="veterinario_id")
	private Veterinario veterinario;
	
	@Column
	private Double valor;
	
	
	@OneToMany(cascade = CascadeType.ALL)
	@JoinColumn(name="exame_id")
	private List<Exame> exames;
	
	public Consulta(){}
	
	public List<Exame> getExames() {
		return exames;
	}



	public void setExames(List<Exame> exames) {
		this.exames = exames;
	}



	public Integer getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}

	public String getResumo() {
		return resumo;
	}

	public void setResumo(String resumo) {
		this.resumo = resumo;
	}

	public Veterinario getVeterinario() {
		return veterinario;
	}

	public void setVeterinario(Veterinario veterinario) {
		this.veterinario = veterinario;
	}

	public Double getValor() {
		return valor;
	}

	public void setValor(Double valor) {
		this.valor = valor;
	}
	
}
