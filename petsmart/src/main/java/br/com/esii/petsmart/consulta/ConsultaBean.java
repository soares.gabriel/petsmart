package br.com.esii.petsmart.consulta;

import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import br.com.esii.petsmart.animal.Animal;
import br.com.esii.petsmart.cliente.Cliente;
import br.com.esii.petsmart.veterinario.Veterinario;

@ManagedBean(name="consultaBean")
@SessionScoped
public class ConsultaBean {
		
	private Cliente cliente;
	private Veterinario veterinario;
	private Animal animal;
	private Consulta consulta;
	private Consulta consultaSelecionada;
	
	
	/*Iniciando todos os objetos na chamada do Bean*/
	public ConsultaBean(){
		cliente = new Cliente();
		veterinario = new Veterinario();
		animal = new Animal();
		consulta = new Consulta();
		consultaSelecionada = new Consulta(); 
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public Veterinario getVeterinario() {
		return veterinario;
	}

	public void setVeterinario(Veterinario veterinario) {
		this.veterinario = veterinario;
	}

	public Animal getAnimal() {
		return animal;
	}

	public void setAnimal(Animal animal) {
		this.animal = animal;
	}

	public Consulta getConsulta() {
		return consulta;
	}

	public void setConsulta(Consulta consulta) {
		this.consulta = consulta;
	}
	
	
	
	public Consulta getConsultaSelecionada() {
		return consultaSelecionada;
	}

	public void setConsultaSelecionada(Consulta consultaSelecionada) {
		this.consultaSelecionada = consultaSelecionada;
	}

	/*Salva uma conssulta */
	public String salvar(){
		ConsultaRN consultaRN = new ConsultaRN();
		consultaRN.salvar(this.consulta);
        this.consulta = new Consulta();
		FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "", "Consulta cadastrado com sucesso."));
		return "/restrito/clinica/consulta/index";
	}
	
	/*Lista todas a consultas registradas no sistema*/
	public List<Consulta> listarTodos(){
		ConsultaRN consultaRN = new ConsultaRN();
       return consultaRN.listar();
	}
	
	/*Deleta uma consulta selecionada*/
	public String deletar(){
		ConsultaRN consultaRN = new ConsultaRN();
		consultaRN.excluir(consultaSelecionada);
		FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "", "Consulta deletado com sucesso."));
		return "/restrito/clinica/consulta/index";
	}
	
	/*Altera uma consulta selecionada*/
	public String altarar(){
		ConsultaRN consultaRN = new ConsultaRN();
		consultaRN.atualizar(consultaSelecionada);
		FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "", "Consulta alterado com sucesso."));
		return "/restrito/clinica/consulta/index";
	}

}
