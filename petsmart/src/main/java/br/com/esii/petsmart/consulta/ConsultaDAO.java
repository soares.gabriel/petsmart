package br.com.esii.petsmart.consulta;

import java.util.List;

public interface ConsultaDAO {
	
	public void salvar(Consulta consulta);
	public void atualizar(Consulta consulta);
	public void excluir(Consulta consulta);
	public Consulta carregar(Integer id);
	public List<Consulta> listar();

}
