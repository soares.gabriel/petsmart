package br.com.esii.petsmart.consulta;

import java.util.List;

import org.hibernate.Session;

import br.com.esii.petsmart.cliente.Cliente;

public class ConsultaDAOHibernate implements ConsultaDAO {
	
private Session session;
	
	public void setSession(Session session){
		this.session = session;
	}

	@Override
	public void salvar(Consulta consulta) {
		this.session.save(consulta);
		
	}

	@Override
	public void atualizar(Consulta consulta) {
		this.session.update(consulta);
		
	}

	@Override
	public void excluir(Consulta consulta) {
		this.session.delete(consulta);
		
	}

	@Override
	public Consulta carregar(Integer id) {
		return (Consulta) this.session.get(Consulta.class, id);
	}

	@SuppressWarnings({ "unchecked" })
	@Override
	public List<Consulta> listar() {
		return this.session.createCriteria(Consulta.class).list();
	}
	
	
	

}
