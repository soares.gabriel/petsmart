package br.com.esii.petsmart.consulta;

import java.util.List;
import br.com.esii.petsmart.util.DAOFactory;


public class ConsultaRN {
	
	private ConsultaDAO consultaDAO;
	
	public ConsultaRN() {
		this.consultaDAO = DAOFactory.criarConsultaDAO();
	}

	public Consulta carregar(Integer id) {
		return this.consultaDAO.carregar(id);
	}

	public void salvar(Consulta consulta) {
		this.consultaDAO.salvar(consulta);
	}

	public void atualizar(Consulta consulta) {
         this.consultaDAO.atualizar(consulta);
	}

	public void excluir(Consulta consulta) {
		this.consultaDAO.excluir(consulta);
	}

	public List<Consulta> listar() {
		return this.consultaDAO.listar();
	}

}
