package br.com.esii.petsmart.conversores;

import java.io.Serializable;
import java.util.Map;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import br.com.esii.petsmart.cargo.Cargo;
import br.com.esii.petsmart.entidades.EntidadeAbst;


@FacesConverter(value = "entidadeConverterCargo", forClass = EntidadeAbst.class)
public class EntidadeConverterCargo implements Converter , Serializable{

	private static final long serialVersionUID = -1866102177947131322L;

	@Override
    public Object getAsObject(FacesContext ctx, UIComponent component, String value) {
		if (value != null && !value.isEmpty()) {
            return (Cargo) component.getAttributes().get(value);
        }
        return null;
    }

    @Override
    public String getAsString(FacesContext ctx, UIComponent component, Object value) {
    	if(value instanceof Cargo){
    		Cargo cargo = (Cargo) value;
            if (cargo != null && cargo instanceof Cargo && cargo.getId()!= null) {
                component.getAttributes().put(cargo.getId().toString(), cargo);
                return cargo.getId().toString();
         	}
    	}
    	return "";
    }

    protected void addAttribute(UIComponent component, EntidadeAbst o) {
        String key = o.getId().toString();
        this.getAttributesFrom(component).put(key, o);
    }

    protected Map<String, Object> getAttributesFrom(UIComponent component) {
        return component.getAttributes();
    }

}
