package br.com.esii.petsmart.conversores;

import java.io.Serializable;
import java.util.Map;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import br.com.esii.petsmart.entidades.EntidadeAbst;
import br.com.esii.petsmart.especie.Especie;


@FacesConverter(value = "entidadeConverterEspecie", forClass = EntidadeAbst.class)
public class EntidadeConverterEspecie implements Converter , Serializable{

	private static final long serialVersionUID = -1866102177947131322L;

	@Override
    public Object getAsObject(FacesContext ctx, UIComponent component, String value) {
		if (value != null && !value.isEmpty()) {
            return (Especie) component.getAttributes().get(value);
        }
        return null;
    }

    @Override
    public String getAsString(FacesContext ctx, UIComponent component, Object value) {
    	//return value == null ? "" : value.toString();
    	if(value instanceof Especie){
    		Especie especie = (Especie) value;
            if (especie != null && especie instanceof Especie && especie.getId()!= null) {
                component.getAttributes().put(especie.getId().toString(), especie);
                return especie.getId().toString();
         	}
    	}
    	return "";
    }

    protected void addAttribute(UIComponent component, EntidadeAbst o) {
        String key = o.getId().toString();
        this.getAttributesFrom(component).put(key, o);
    }

    protected Map<String, Object> getAttributesFrom(UIComponent component) {
        return component.getAttributes();
    }

}
