package br.com.esii.petsmart.conversores;

import java.io.Serializable;
import java.util.Map;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import br.com.esii.petsmart.entidades.EntidadeAbst;
import br.com.esii.petsmart.fornecedor.Fornecedor;


@FacesConverter(value = "entidadeConverterFornecedor", forClass = EntidadeAbst.class)
public class EntidadeConverterFornecedor implements Converter , Serializable{

	private static final long serialVersionUID = -1866102177947131322L;

	@Override
    public Object getAsObject(FacesContext ctx, UIComponent component, String value) {
		if (value != null && !value.isEmpty()) {
            return (Fornecedor) component.getAttributes().get(value);
        }
        return null;
    }

    @Override
    public String getAsString(FacesContext ctx, UIComponent component, Object value) {
    	if(value instanceof Fornecedor){
    		Fornecedor fornecedor = (Fornecedor) value;
            if (fornecedor != null && fornecedor instanceof Fornecedor && fornecedor.getId()!= null) {
                component.getAttributes().put(fornecedor.getId().toString(), fornecedor);
                return fornecedor.getId().toString();
         	}
    	}
    	return "";
    }

    protected void addAttribute(UIComponent component, EntidadeAbst o) {
        String key = o.getId().toString();
        this.getAttributesFrom(component).put(key, o);
    }

    protected Map<String, Object> getAttributesFrom(UIComponent component) {
        return component.getAttributes();
    }

}
