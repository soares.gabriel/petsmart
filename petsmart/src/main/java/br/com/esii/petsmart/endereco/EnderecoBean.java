package br.com.esii.petsmart.endereco;

import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;


@ManagedBean(name="enderecoBean")
@SessionScoped
public class EnderecoBean {

	private Endereco endereco;
	private Endereco enderecoSelecionado;
	
	public EnderecoBean(){
		endereco = new Endereco();
		enderecoSelecionado = new Endereco();
	}

	public Endereco getEndereco() {
		return endereco;
	}

	public void setEndereco(Endereco endereco) {
		this.endereco = endereco;
	}

	public Endereco getEnderecoSelecionado() {
		return enderecoSelecionado;
	}

	public void setEnderecoSelecionado(Endereco enderecoSelecionado) {
		this.enderecoSelecionado = enderecoSelecionado;
	}
	
	
	public void salvar(){
		EnderecoRN enderecoRN = new EnderecoRN();
		enderecoRN.salvar(endereco);
		Endereco enderecoNull = new Endereco();
		this.endereco = enderecoNull;	
	}
	
	public List<Endereco> listarTodos(){
		EnderecoRN enderecoRN = new EnderecoRN();
		return enderecoRN.listar();
	}
	
	public void alterar(){
		EnderecoRN enderecoRN = new EnderecoRN();
		enderecoRN.atualizar(enderecoSelecionado);
		Endereco enderecoNull = new Endereco();
		this.endereco = enderecoNull;	
	}
	
	public void deletar(){
		EnderecoRN enderecoRN = new EnderecoRN();
		enderecoRN.excluir(enderecoSelecionado);
	}
	
}
