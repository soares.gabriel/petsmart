package br.com.esii.petsmart.endereco;

import java.util.List;

public interface EnderecoDAO {
	
	public void salvar(Endereco endereco);
	public void atualizar(Endereco endereco);
	public void excluir(Endereco endereco);
	public Endereco carregar(Integer id);
	public List<Endereco> listar();
	
}
