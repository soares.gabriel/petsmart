package br.com.esii.petsmart.endereco;

import java.util.List;

import org.hibernate.Session;

public class EnderecoDAOHibernate implements EnderecoDAO {

private Session session;
	
	public void setSession(Session session){
		this.session = session;
	}

	@Override
	public void salvar(Endereco endereco) {
		this.session.save(endereco);
	}

	@Override
	public void atualizar(Endereco endereco) {
		this.session.update(endereco);
	}

	@Override
	public void excluir(Endereco endereco) {
		this.session.delete(endereco);
	}

	@Override
	public Endereco carregar(Integer id) {		
		return (Endereco) this.session.get(Endereco.class, id);
	}

	@SuppressWarnings({ "deprecation", "unchecked" })
	@Override
	public List<Endereco> listar() {
	   return this.session.createCriteria(Endereco.class).list();
	}
}
