package br.com.esii.petsmart.especie;

import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

import br.com.esii.petsmart.raca.Raca;
import br.com.esii.petsmart.raca.RacaRN;

@ManagedBean(name="especieBean")
@RequestScoped
public class EspecieBean {
	
	private Especie especie;
	private Especie especieSelecionado;
	
	private List<Especie> lista;
	
	public EspecieBean(){
		this.especie = new Especie();
		this.especieSelecionado = new Especie();
	}

	public Especie getEspecie() {
		return especie;
	}

	public void setEspecie(Especie especie) {
		this.especie = especie;
	}

	public Especie getEspecieSelecionado() {
		return especieSelecionado;
	}

	public void setEspecieSelecionado(Especie especieSelecionado) {
		this.especieSelecionado = especieSelecionado;
	}
	
	public String novo(){
		return "/restrito/clinica/especie/Create";
	}
	
	public String editar(){
		return "/restrito/clinica/especie/Edit";
	}
	
	public String inicial(){
		return "/restrito/clinica/especie/index";
	}

	public String salvar(){
		EspecieRN especieRN = new EspecieRN();
		especieRN.salvar(especie);
		this.especie = new Especie();
		return "/restrito/clinica/especie/index";
	}
	
	public List<Especie> listarTodos(){
		EspecieRN especieRN = new EspecieRN();
		this.lista = especieRN.listar();
		return lista;
	}
	
	public String alterar(){
		EspecieRN especieRN = new EspecieRN();
		especieRN.atualizar(especieSelecionado);
		return "/restrito/clinica/especie/index";
	}
	
	public void deletar(){
		EspecieRN especieRN = new EspecieRN();
		especieRN.excluir(especieSelecionado);
	}
	
	public void carregarRacas(){
		RacaRN racaRN = new RacaRN();
		
		List<Raca> racas = racaRN.listar();
		List<Raca> racasAux = new ArrayList<>();
		for(Raca raca:racas){
			if(raca.getEspecie().getId().equals(especieSelecionado.getId())){
			   racasAux.add(raca);
			}
		}
		
		especieSelecionado.setRacas(racasAux);
	}

}
