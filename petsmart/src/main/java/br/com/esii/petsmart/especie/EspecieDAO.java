package br.com.esii.petsmart.especie;

import java.util.List;

import br.com.esii.petsmart.especie.Especie;

public interface EspecieDAO {
	
	public void salvar(Especie especie);
	public void atualizar(Especie especie);
	public void excluir(Especie especie);
	public Especie carregar(Integer id);
	public List<Especie> listar();
	
}
