package br.com.esii.petsmart.especie;

import java.util.List;

import org.hibernate.Session;


public class EspecieDAOHibernate implements EspecieDAO{

private Session session;
	
	public void setSession(Session session){
		this.session = session;
	}

	@Override
	public void salvar(Especie especie) {
		this.session.save(especie);
	}

	@Override
	public void atualizar(Especie especie) {
		this.session.saveOrUpdate(especie);
	}

	@Override
	public void excluir(Especie especie) {
		this.session.delete(especie);
	}

	@Override
	public Especie carregar(Integer id) {		
		return (Especie) this.session.get(Especie.class, id);
	}

	@SuppressWarnings({ "unchecked" })
	@Override
	public List<Especie> listar() {
	   return this.session.createCriteria(Especie.class).list();
	}

}
