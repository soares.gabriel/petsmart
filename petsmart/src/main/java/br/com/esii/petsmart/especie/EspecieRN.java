package br.com.esii.petsmart.especie;

import java.util.List;
import br.com.esii.petsmart.util.DAOFactory;

public class EspecieRN {
	
	private EspecieDAO especieDAO;
	
	public EspecieRN(){
		this.especieDAO = DAOFactory.criarEspecieDAO();
	}
	
	public Especie carregar(Integer id){
		return this.especieDAO.carregar(id);
	}
	
	public void salvar(Especie especie){
		Integer id = especie.getId();
		
		if((id==null)||(id==0)){
			this.especieDAO.salvar(especie);
		}else{
			this.especieDAO.atualizar(especie);
		}
		
		
	}
	
	public void atualizar(Especie especie){
		this.especieDAO.atualizar(especie);
	}
	
	public void excluir(Especie especie){
		this.especieDAO.excluir(especie);
	}
	
	public List<Especie> listar(){
		return this.especieDAO.listar();
	}

}
