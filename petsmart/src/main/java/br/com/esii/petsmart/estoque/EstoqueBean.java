package br.com.esii.petsmart.estoque;


import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

@ManagedBean(name="estoqueBean")
@SessionScoped
public class EstoqueBean {
	
	private Estoque estoque;
	private Estoque estoqueSeleconado;
	
	public EstoqueBean(){
		estoque = new Estoque();
		estoqueSeleconado = null;
		
	}

	public Estoque getEstoque() {
		return estoque;
	}

	public void setEstoque(Estoque estoque) {
		this.estoque = estoque;
	}
	
	public Estoque getEstoqueSeleconado() {
		return estoqueSeleconado;
	}

	public void setEstoqueSeleconado(Estoque estoqueSeleconado) {
		this.estoqueSeleconado = estoqueSeleconado;
	}
	
	public String novo(){
		return "/restrito/almoxarifado/estoque/Create";
	}
	
	public String editar(){
		return "/restrito/almoxarifado/estoque/Edit";
	}
	
	public String inicial(){
		return "/restrito/almoxarifado/estoque/index";
	}
	


	public String salvar(){
		EstoqueRN estoqueRN = new EstoqueRN();
		estoqueRN.salvar(estoque);
		Estoque estoque = new Estoque();
		this.estoque = estoque;
		FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "", "Criado com sucesso."));
		return "/restrito/almoxarifado/estoque/index";
	}
	
	public List<Estoque> listarTodos(){
		 EstoqueRN estoqueRN = new EstoqueRN();
		return estoqueRN.listar();
	}
	
	public void deletar(){
		EstoqueRN estoqueRN = new EstoqueRN();
		estoqueRN.excluir(estoqueSeleconado);
		FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "", "Deletado com sucesso."));
	}
	
	public String alterar(){
		EstoqueRN estoqueRN = new EstoqueRN();
		estoqueRN.atualizar(estoqueSeleconado);
		Estoque estoque = new Estoque();
		this.estoqueSeleconado = estoque;
		FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "", "Alterado com sucesso."));
		return "/restrito/almoxarifado/estoque/index";
	}

}
