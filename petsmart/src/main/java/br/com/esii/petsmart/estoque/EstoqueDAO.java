package br.com.esii.petsmart.estoque;

import java.util.List;

public interface EstoqueDAO {
	
	public void salvar(Estoque estoque);
	public void atualizar(Estoque estoque);
	public void excluir(Estoque estoque);
	public Estoque carregar(Integer id);
	public List<Estoque> listar();

}
