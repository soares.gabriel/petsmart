package br.com.esii.petsmart.estoque;

import java.util.List;

import org.hibernate.Session;

import br.com.esii.petsmart.estoque.Estoque;

public class EstoqueDAOHibernate implements EstoqueDAO{
	
	private Session session;
	
	public void setSession(Session session){
		this.session = session;
	}

	@Override
	public void salvar(Estoque estoque) {
		this.session.save(estoque);
	}

	@Override
	public void atualizar(Estoque estoque) {
		this.session.saveOrUpdate(estoque);
	}

	@Override
	public void excluir(Estoque estoque) {
		this.session.delete(estoque);
	}

	@Override
	public Estoque carregar(Integer id) {		
		return (Estoque) this.session.get(Estoque.class, id);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Estoque> listar() {
	   return this.session.createCriteria(Estoque.class).list();
	}

	


}
