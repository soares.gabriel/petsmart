package br.com.esii.petsmart.estoque;

import java.util.List;

import br.com.esii.petsmart.util.DAOFactory;

public class EstoqueRN {
	
	private EstoqueDAO estoqueDAO;
	
	public EstoqueRN(){
		this.estoqueDAO = DAOFactory.criarEstoqueDAO();
	}
	
	public Estoque carregar(Integer id){
		return this.estoqueDAO.carregar(id);
	}
	
	public void salvar(Estoque estoque){
		this.estoqueDAO.salvar(estoque);
	
	}
	
	public void atualizar(Estoque estoque){
		this.estoqueDAO.atualizar(estoque);
	}
	
	public void excluir(Estoque estoque){
		this.estoqueDAO.excluir(estoque);
	}
	
	public List<Estoque> listar(){
		return this.estoqueDAO.listar();
	}

}
