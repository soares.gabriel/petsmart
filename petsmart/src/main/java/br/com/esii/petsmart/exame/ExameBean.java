package br.com.esii.petsmart.exame;

import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;


@ManagedBean(name="exameBean")
@SessionScoped
public class ExameBean {
	
	private Exame exame;
	private Exame exameSelecionado;

	
	public ExameBean(){
		this.exame = new Exame();
		this.exameSelecionado = new Exame();
	}

	public Exame getExame() {
		return exame;
	}

	public void setExame(Exame exame) {
		this.exame = exame;
	}

	public Exame getExameSelecionado() {
		return exameSelecionado;
	}

	public void setExameSelecionado(Exame exameSelecionado) {
		this.exameSelecionado = exameSelecionado;
	}

	public String salvar(){
		ExameRN exameRN = new ExameRN();
		exameRN.salvar(exame);
        this.exame = new Exame();
		FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "", "Exame cadastrado com sucesso."));
		return "/restrito/clinica/exame/index";
	}
	
	public List<Exame> listarTodos(){
		ExameRN exameRN = new ExameRN();
       return exameRN.listar();
	}
	
	public String deletar(){
		ExameRN exameRN = new ExameRN();
		exameRN.excluir(exameSelecionado);
		FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "", "Exame deletado com sucesso."));
		return "/restrito/clinica/exame/index";
	}
	
	public String alterar(){
		ExameRN exameRN = new ExameRN();
		exameRN.atualizar(exameSelecionado);
		FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "", "Exame alterado com sucesso."));
		return "/restrito/clinica/exame/index";
	}

}
