package br.com.esii.petsmart.exame;

import java.util.List;

public interface ExameDAO {
	
		public void salvar(Exame exame);
		public void atualizar(Exame exame);
		public void excluir(Exame exame);
		public Exame carregar(Integer id);
		public List<Exame> listar();

}
