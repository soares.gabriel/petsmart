package br.com.esii.petsmart.exame;

import java.util.List;

import org.hibernate.Session;

public class ExameDAOHibernate implements ExameDAO {
	
private Session session;
	
	public void setSession(Session session){
		this.session = session;
	}


	@Override
	public void salvar(Exame exame) {
		this.session.save(exame);
	}

	@Override
	public void atualizar(Exame exame) {
		this.session.update(exame);;	
	}

	@Override
	public void excluir(Exame exame) {
		this.session.delete(exame);	
	}

	@Override
	public Exame carregar(Integer id) {
		return (Exame) this.session.get(Exame.class, id);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Exame> listar() {
		return this.session.createCriteria(Exame.class).list();
	}

}
