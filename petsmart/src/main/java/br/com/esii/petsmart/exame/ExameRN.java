package br.com.esii.petsmart.exame;

import java.util.List;

import br.com.esii.petsmart.util.DAOFactory;

public class ExameRN {
	
	private ExameDAO exameDAO;
	
	public ExameRN(){
		this.exameDAO = DAOFactory.criarExameDAO();
	}

	public Exame carregar(Integer id){
		return this.exameDAO.carregar(id);
	}
	
	public void salvar(Exame exame){
		this.exameDAO.salvar(exame);
	
	}
	
	public void atualizar(Exame exame){
		this.exameDAO.atualizar(exame);
	}
	
	public void excluir(Exame fornecedor){
		this.exameDAO.excluir(fornecedor);
	}
	
	public List<Exame> listar(){
		return this.exameDAO.listar();
	}
}
