package br.com.esii.petsmart.fornecedor;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import br.com.esii.petsmart.endereco.Endereco;
import br.com.esii.petsmart.entidades.EntidadeAbst;
import br.com.esii.petsmart.produto.Produto;

@Entity
@Table(name="fornecedor")
public class Fornecedor extends EntidadeAbst implements Serializable{
	
	private static final long serialVersionUID = 5055837730284487555L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO) 
	@Column(name="id_fornecedor")
	private Integer id;
	
	@Column(name="codigo")
	private String codigo;
	
	@Column(name="nome_fantasia")
	private String nomeFantasia;
	
	@Column(name="nome_real")
	private String nomeReal;
	
	@Column(name="responsavel")
	private String Responsavel;
	
	@OneToOne(mappedBy="fornecedor",targetEntity = Endereco.class, fetch=FetchType.LAZY)
	private Endereco endereco;
	
	@Column(name="telefone")
	private String telefone;
	 
	@Column(name="cnpj")
	private String cnpj;

	@ManyToMany(cascade=CascadeType.ALL) 
	@JoinTable(name="fornecedores_produtos", joinColumns=  @JoinColumn( name = "id_fornecedor"), inverseJoinColumns= @JoinColumn(name = "id_produto"))
	private Set<Produto> produtos;
	
	public Fornecedor() {}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getNomeFantasia() {
		return nomeFantasia;
	}

	public void setNomeFantasia(String nomeFantasia) {
		this.nomeFantasia = nomeFantasia;
	}

	public String getNomeReal() {
		return nomeReal;
	}

	public void setNomeReal(String nomeReal) {
		this.nomeReal = nomeReal;
	}

	public String getResponsavel() {
		return Responsavel;
	}

	public void setResponsavel(String responsavel) {
		Responsavel = responsavel;
	}

	public Endereco getEndereco() {
		if(endereco == null){
			this.endereco = new Endereco();
		}
		return endereco;
	}

	public void setEndereco(Endereco endereco) {
		this.endereco = endereco;
	}

	public String getTelefone() {
		return telefone;
	}

	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}

	public String getCnpj() {
		return cnpj;
	}

	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}

	public Set<Produto> getProdutos() {
		return produtos;
	}

	public void setProdutos(Set<Produto> produtos) {
		this.produtos = produtos;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((Responsavel == null) ? 0 : Responsavel.hashCode());
		result = prime * result + ((cnpj == null) ? 0 : cnpj.hashCode());
		result = prime * result + ((codigo == null) ? 0 : codigo.hashCode());
		result = prime * result + ((endereco == null) ? 0 : endereco.hashCode());
		result = prime * result + id;
		result = prime * result + ((nomeFantasia == null) ? 0 : nomeFantasia.hashCode());
		result = prime * result + ((nomeReal == null) ? 0 : nomeReal.hashCode());
		result = prime * result + ((produtos == null) ? 0 : produtos.hashCode());
		result = prime * result + ((telefone == null) ? 0 : telefone.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Fornecedor other = (Fornecedor) obj;
		if (Responsavel == null) {
			if (other.Responsavel != null)
				return false;
		} else if (!Responsavel.equals(other.Responsavel))
			return false;
		if (cnpj == null) {
			if (other.cnpj != null)
				return false;
		} else if (!cnpj.equals(other.cnpj))
			return false;
		if (codigo == null) {
			if (other.codigo != null)
				return false;
		} else if (!codigo.equals(other.codigo))
			return false;
		if (endereco == null) {
			if (other.endereco != null)
				return false;
		} else if (!endereco.equals(other.endereco))
			return false;
		if (id != other.id)
			return false;
		if (nomeFantasia == null) {
			if (other.nomeFantasia != null)
				return false;
		} else if (!nomeFantasia.equals(other.nomeFantasia))
			return false;
		if (nomeReal == null) {
			if (other.nomeReal != null)
				return false;
		} else if (!nomeReal.equals(other.nomeReal))
			return false;
		if (produtos == null) {
			if (other.produtos != null)
				return false;
		} else if (!produtos.equals(other.produtos))
			return false;
		if (telefone == null) {
			if (other.telefone != null)
				return false;
		} else if (!telefone.equals(other.telefone))
			return false;
		return true;
	}

	
}
