package br.com.esii.petsmart.fornecedor;

import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import br.com.esii.petsmart.endereco.Endereco;
import br.com.esii.petsmart.endereco.EnderecoRN;
import br.com.esii.petsmart.estoque.EstoqueRN;

@ManagedBean(name="fornecedorBean")
@SessionScoped
public class FornecedorBean {
	
	private Fornecedor fornecedor;
	private Fornecedor fornecedorSelecionado;
	
	public FornecedorBean(){
		this.fornecedor = new Fornecedor();
		this.fornecedorSelecionado = new Fornecedor();
	}

	public Fornecedor getFornecedor() {
		return fornecedor;
	}

	public void setFornecedor(Fornecedor fornecedor) {
		this.fornecedor = fornecedor;
	}

	public Fornecedor getFornecedorSelecionado() {
		return fornecedorSelecionado;
	}

	public void setFornecedorSelecionado(Fornecedor fornecedorSelecionado) {
		this.fornecedorSelecionado = fornecedorSelecionado;
	}
	
	public String cadastrarProduto(){
		return "/restrito/fornecedor/produtoFornecedor";
	}
	
	public String salvar(){
		FornecedorRN fornecedorRN = new FornecedorRN();
		EnderecoRN enderecoRN = new EnderecoRN();
		
		Endereco endereco = fornecedor.getEndereco();
		endereco.setFornecedor(fornecedor);
		enderecoRN.salvar(endereco);
		
		fornecedor.setEndereco(endereco);
		
		fornecedorRN.salvar(fornecedor);
	   
		this.fornecedor = new Fornecedor();
		
		FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "", "Fornecedor cadastrado com sucesso."));
		return "/restrito/fornecedor/index";
	}
	
	public List<Fornecedor> listarTodos(){
		FornecedorRN fornecedorRN = new FornecedorRN();
		return fornecedorRN.listar();
	}
	
	public void deletar(){
		FornecedorRN fornecedorRN = new FornecedorRN();
		EnderecoRN enderecoRN = new EnderecoRN();
		
		enderecoRN.excluir(fornecedorSelecionado.getEndereco());
		fornecedorRN.excluir(fornecedorSelecionado);
		this.fornecedorSelecionado = new Fornecedor();
		
		FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "", "Deletado com sucesso."));
	}
	
	public String alterar(){
		FornecedorRN fornecedorRN = new FornecedorRN();
		EnderecoRN enderecoRN = new EnderecoRN();
		
		enderecoRN.atualizar(fornecedorSelecionado.getEndereco());
	
		fornecedorRN.atualizar(fornecedorSelecionado);
	   
		this.fornecedorSelecionado = new Fornecedor();
	
		FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "", "Fornecedor alterado com sucesso."));
		return "/restrito/fornecedor/index";
	}

}
