package br.com.esii.petsmart.fornecedor;

import java.util.List;

public interface FornecedorDAO {
	
		public void salvar(Fornecedor fornecedor);
		public void atualizar(Fornecedor fornecedor);
		public void excluir(Fornecedor fornecedor);
		public Fornecedor carregar(Integer id);
		public List<Fornecedor> listar();

}
