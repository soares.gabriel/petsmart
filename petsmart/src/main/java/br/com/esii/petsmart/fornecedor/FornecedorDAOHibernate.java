package br.com.esii.petsmart.fornecedor;

import java.util.List;

import org.hibernate.Session;

public class FornecedorDAOHibernate implements FornecedorDAO {
	
private Session session;
	
	public void setSession(Session session){
		this.session = session;
	}


	@Override
	public void salvar(Fornecedor fornecedor) {
		this.session.save(fornecedor);
	}

	@Override
	public void atualizar(Fornecedor fornecedor) {
		this.session.update(fornecedor);;	
	}

	@Override
	public void excluir(Fornecedor fornecedor) {
		this.session.delete(fornecedor);	
	}

	@Override
	public Fornecedor carregar(Integer id) {
		return (Fornecedor) this.session.get(Fornecedor.class, id);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Fornecedor> listar() {
		return this.session.createCriteria(Fornecedor.class).list();
	}

}
