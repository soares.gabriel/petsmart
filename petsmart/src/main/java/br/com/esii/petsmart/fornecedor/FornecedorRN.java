package br.com.esii.petsmart.fornecedor;

import java.util.List;

import br.com.esii.petsmart.util.DAOFactory;

public class FornecedorRN {
	
	private FornecedorDAO fornecedorDAO;
	
	public FornecedorRN(){
		this.fornecedorDAO = DAOFactory.criarFornecedorDAO();
	}

	public Fornecedor carregar(Integer id){
		return this.fornecedorDAO.carregar(id);
	}
	
	public void salvar(Fornecedor fornecedor){
		this.fornecedorDAO.salvar(fornecedor);
	
	}
	
	public void atualizar(Fornecedor fornecedor){
		this.fornecedorDAO.atualizar(fornecedor);
	}
	
	public void excluir(Fornecedor fornecedor){
		this.fornecedorDAO.excluir(fornecedor);
	}
	
	public List<Fornecedor> listar(){
		return this.fornecedorDAO.listar();
	}
}
