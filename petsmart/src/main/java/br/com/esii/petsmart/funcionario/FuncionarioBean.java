package br.com.esii.petsmart.funcionario;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import br.com.esii.petsmart.endereco.Endereco;

@ManagedBean(name="funcionarioBean")
@SessionScoped
public class FuncionarioBean {
	
	private Funcionario funcionario;
	private Funcionario funcionarioSelecionado;
	private Endereco endereco;

	
	public FuncionarioBean(){
		this.funcionario = new Funcionario();
		this.funcionarioSelecionado = new Funcionario();
		this.endereco = new Endereco();
	}
	
	 @PostConstruct
	    public void init() {
		 this.funcionario = new Funcionario();
			this.funcionarioSelecionado = new Funcionario();
			this.endereco = new Endereco();
	 }


	public Funcionario getFuncionario() {
		return funcionario;
	}


	public void setFuncionario(Funcionario funcionario) {
		this.funcionario = funcionario;
	}


	public Funcionario getFuncionarioSelecionado() {
		return funcionarioSelecionado;
	}


	public void setFuncionarioSelecionado(Funcionario funcionarioSelecionado) {
		this.funcionarioSelecionado = funcionarioSelecionado;
	}

	public Endereco getEndereco() {
		if(endereco == null){
			this.endereco = new Endereco();
		}
		return endereco;
	}


	public void setEndereco(Endereco endereco) {
		this.endereco = endereco;
	}


	public String salvar(){
		FuncionarioRN funcionarioRN = new FuncionarioRN();
		funcionarioRN.salvar(funcionario);
        this.funcionario = new Funcionario();
		FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "", "Funcionario cadastrado com sucesso."));
		return "/restrito/administracao/funcionario/index";
	}
	
	public List<Funcionario> listarTodos(){
	   FuncionarioRN funcionarioRN = new FuncionarioRN();
       return funcionarioRN.listar();
	}
	
	public String deletar(){
		FuncionarioRN funcionarioRN = new FuncionarioRN();
		funcionarioRN.excluir(funcionarioSelecionado);
		FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "", "Funcionario deletado com sucesso."));
		return "/restrito/clinica/exame/index";
	}
	
	public String alterar(){
		FuncionarioRN funcionarioRN = new FuncionarioRN();
		funcionarioRN.atualizar(funcionarioSelecionado);
		FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "", "Funcionario alterado com sucesso."));
		return "/restrito/clinica/exame/index";
	}

}
