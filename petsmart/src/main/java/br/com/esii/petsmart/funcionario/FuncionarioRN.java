package br.com.esii.petsmart.funcionario;

import java.util.List;

import br.com.esii.petsmart.util.DAOFactory;

public class FuncionarioRN {
	
	private FuncionarioDAO funcionarioDAO;
	
	public FuncionarioRN(){
		this.funcionarioDAO = DAOFactory.criarFuncionarioDAO();
	}

	public Funcionario carregar(Integer id){
		return this.funcionarioDAO.carregar(id);
	}
	
	public void salvar(Funcionario funcionario){
		this.funcionarioDAO.salvar(funcionario);
	
	}
	
	public void atualizar(Funcionario funcionario){
		this.funcionarioDAO.atualizar(funcionario);
	}
	
	public void excluir(Funcionario fornecedor){
		this.funcionarioDAO.excluir(fornecedor);
	}
	
	public List<Funcionario> listar(){
		return this.funcionarioDAO.listar();
	}
}
