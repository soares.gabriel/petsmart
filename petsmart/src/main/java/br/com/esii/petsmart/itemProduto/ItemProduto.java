package br.com.esii.petsmart.itemProduto;

import java.io.Serializable;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import br.com.esii.petsmart.produto.Produto;

@Entity
@Table(name="item_produto")
public class ItemProduto implements Serializable{

	private static final long serialVersionUID = -245126258979219018L;
	
	@Id
	@GeneratedValue
	@Column(name="id_itemProduto")
	private int id;
	
	@OneToMany(cascade=CascadeType.ALL, mappedBy = "itemProduto")
	private Set<Produto> produtos;
	
	@Column(name="valor_revenda")
	private double valorRevenda;
	
	@Column
	private int quantidade;
	
	public ItemProduto(){}

	public int getId() {
		return id;
	}

	public Set<Produto> getProdutos() {
		return produtos;
	}

	public void setProdutos(Set<Produto> produtos) {
		this.produtos = produtos;
	}

	public double getValorRevenda() {
		return valorRevenda;
	}

	public void setValorRevenda(double valorRevenda) {
		this.valorRevenda = valorRevenda;
	}

	public int getQuantidade() {
		return quantidade;
	}

	public void setQuantidade(int quantidade) {
		this.quantidade = quantidade;
	}

	public void setId(int id) {
		this.id = id;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		result = prime * result + ((produtos == null) ? 0 : produtos.hashCode());
		result = prime * result + quantidade;
		long temp;
		temp = Double.doubleToLongBits(valorRevenda);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ItemProduto other = (ItemProduto) obj;
		if (id != other.id)
			return false;
		if (produtos == null) {
			if (other.produtos != null)
				return false;
		} else if (!produtos.equals(other.produtos))
			return false;
		if (quantidade != other.quantidade)
			return false;
		if (Double.doubleToLongBits(valorRevenda) != Double.doubleToLongBits(other.valorRevenda))
			return false;
		return true;
	}

	
}
