package br.com.esii.petsmart.itemProduto;

import java.util.List;

public interface ItemProdutoDAO {
	public void salvar(ItemProduto itemProduto);
	public void atualizar(ItemProduto itemProduto);
	public void excluir(ItemProduto itemProduto);
	public ItemProduto carregar(Integer id);
	public List<ItemProduto> listar();
}
