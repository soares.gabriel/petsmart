package br.com.esii.petsmart.itemProduto;

import java.util.List;

import org.hibernate.Session;


public class ItemProdutoDAOHibernate  implements ItemProdutoDAO {
	
	private Session session;
	
	public void setSession(Session session){
		this.session = session;
	}

	@Override
	public void salvar(ItemProduto itemProduto) {
		this.session.save(itemProduto);
	}

	@Override
	public void atualizar(ItemProduto itemProduto) {
		this.session.update(itemProduto);
	}

	@Override
	public void excluir(ItemProduto itemProduto) {
		this.session.delete(itemProduto);
	}

	@Override
	public ItemProduto carregar(Integer id) {
		return (ItemProduto) this.session.get(ItemProduto.class, id);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<ItemProduto> listar() {
		return this.session.createCriteria(ItemProduto.class).list();
	}

}
