package br.com.esii.petsmart.itemProduto;

import java.util.List;

import br.com.esii.petsmart.util.DAOFactory;

public class ItemProdutoRN {
	
	private ItemProdutoDAO itemProdutoDAO;
	
	public ItemProdutoRN(){
		this.itemProdutoDAO = DAOFactory.criarItemProdutoDAO();
	}
	
	  public ItemProduto carregar(Integer id){
			return this.itemProdutoDAO.carregar(id);
		}
		
		public void salvar(ItemProduto itemProduto){
		   this.itemProdutoDAO.salvar(itemProduto);
		}
		
		public void atualizar(ItemProduto itemProduto){
			this.itemProdutoDAO.atualizar(itemProduto);
		}
		
		public void excluir(ItemProduto itemProduto){
			this.itemProdutoDAO.excluir(itemProduto);
		}
		
		public List<ItemProduto> listar(){
			return this.itemProdutoDAO.listar();
		}

}
