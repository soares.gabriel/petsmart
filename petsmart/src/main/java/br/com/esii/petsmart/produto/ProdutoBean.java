package br.com.esii.petsmart.produto;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import br.com.esii.petsmart.fornecedor.Fornecedor;
import br.com.esii.petsmart.fornecedor.FornecedorRN;

@ManagedBean(name="produtoBean")
@SessionScoped
public class ProdutoBean {
	
	private Produto produto;
	private Produto produtoSelecionado;
	private Fornecedor fornecedor;
	
	public ProdutoBean(){
		this.produto = new Produto();
		this.produtoSelecionado = new Produto();
		this.fornecedor = new Fornecedor();
	}

	public Produto getProduto() {
		if(produto == null){
			this.produto = new Produto();
		}
		return produto;
	}

	public void setProduto(Produto produto) {
		this.produto = produto;
	}

	public Produto getProdutoSelecionado() {
		if(produtoSelecionado == null){
			this.produtoSelecionado = new Produto();
		}
		return produtoSelecionado;
	}

	public void setProdutoSelecionado(Produto produtoSelecionado) {
		this.produtoSelecionado = produtoSelecionado;
	}
	
	
	public Fornecedor getFornecedor() {
		return fornecedor;
	}

	public void setFornecedor(Fornecedor fornecedor) {
		this.fornecedor = fornecedor;
	}

	public String salvar(){
		FornecedorRN fornecedorRN = new FornecedorRN();
		
		fornecedor.getProdutos().add(produto);
		
		fornecedorRN.atualizar(fornecedor);

		this.fornecedor = new Fornecedor();
		this.produto = new Produto();
		FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "", "Produto do fornecedor cadastrado com sucesso."));
		return "/restrito/fornecedor/index";
	}
	
	public List<Produto> listarTodos(){
		ProdutoRN produtoRN = new ProdutoRN();
		return produtoRN.listar();
	}
	
	public void deletar(){
		ProdutoRN produtoRN = new ProdutoRN();
		
		produtoRN.excluir(produtoSelecionado);
		this.produtoSelecionado = new Produto();
		
		FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "", "Produto do fornecedor deletado com sucesso."));
	}
	
	public String alterar(){
		FornecedorRN fornecedorRN = new FornecedorRN();
	
		FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "", "Produto do fornecedor alterado com sucesso."));
		return "/restrito/fornecedor/index";
	}

}
