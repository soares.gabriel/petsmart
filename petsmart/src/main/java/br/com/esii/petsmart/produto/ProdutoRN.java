package br.com.esii.petsmart.produto;

import java.util.List;

import br.com.esii.petsmart.util.DAOFactory;

public class ProdutoRN {
	
private ProdutoDAO produtoDAO;
	
   public ProdutoRN() {
	   this.produtoDAO = DAOFactory.criarProdutoDAO();
	}
   
   public Produto carregar(Integer id){
		return this.produtoDAO.carregar(id);
	}
	
	public void salvar(Produto produto){
	   this.produtoDAO.salvar(produto);
	}
	
	public void atualizar(Produto produto){
		this.produtoDAO.atualizar(produto);
	}
	
	public void excluir(Produto produto){
		this.produtoDAO.excluir(produto);
	}
	
	public List<Produto> listar(){
		return this.produtoDAO.listar();
	}

}
