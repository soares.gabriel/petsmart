package br.com.esii.petsmart.raca;

import java.util.ArrayList;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

@ManagedBean(name="racaBean")
@SessionScoped
public class RacaBean {

	private Raca raca;
	private Raca racaSelecionado;
	
	public RacaBean(){	
		raca = new Raca();
		racaSelecionado = new Raca();
	}

	public Raca getRaca() {
		return raca;
	}

	public void setRaca(Raca raca) {
		this.raca = raca;
	}

	public Raca getRacaSelecionado() {
		return racaSelecionado;
	}

	public void setRacaSelecionado(Raca racaSelecionado) {
		this.racaSelecionado = racaSelecionado;
	}
	
	public String novo(){
		return "/restrito/clinica/raca/Create";
	}
	
	public String editar(){
		return "/restrito/clinica/raca/Edit";
	}
	
	public String inicial(){
		return "/restrito/clinica/raca/index";
	}

	public String salvar(){
		RacaRN racaRN = new RacaRN();
		racaRN.salvar(raca);
		raca = new Raca();
		FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "", "Criado com sucesso."));
		return "/restrito/clinica/raca/index";
	}
	
	public List<Raca> listarTodos(){
		RacaRN racaRN = new RacaRN();
		return racaRN.listar();
	}
	
	public String alterar(){
		RacaRN racaRN = new RacaRN();
		racaRN.atualizar(racaSelecionado);
		FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "", "Alterado com sucesso."));
		return "/restrito/clinica/especie/index";
	}
	
	public void deletar(){
		RacaRN racaRN = new RacaRN();
		racaRN.excluir(racaSelecionado);
		FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "", "Deletado com sucesso."));
	}
	
}
