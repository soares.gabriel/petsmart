package br.com.esii.petsmart.raca;

import java.util.List;

public interface RacaDAO {
	
	public void salvar(Raca raca);
	public void atualizar(Raca raca);
	public void excluir(Raca raca);
	public Raca carregar(Integer id);
	public List<Raca> listar();

}
