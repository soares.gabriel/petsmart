package br.com.esii.petsmart.raca;

import java.util.List;

import org.hibernate.Session;

public class RacaDAOHibernate implements RacaDAO{
	
private Session session;
	
	public void setSession(Session session){
		this.session = session;
	}

	@Override
	public void salvar(Raca raca) {
		this.session.save(raca);
	}

	@Override
	public void atualizar(Raca raca) {
		this.session.update(raca);
	}

	@Override
	public void excluir(Raca raca) {
		this.session.delete(raca);
	}

	@Override
	public Raca carregar(Integer id) {		
		return (Raca) this.session.get(Raca.class, id);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Raca> listar() {
	   return this.session.createCriteria(Raca.class).list();
	}


}
