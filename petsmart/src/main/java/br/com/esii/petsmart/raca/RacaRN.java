package br.com.esii.petsmart.raca;

import java.util.List;


import br.com.esii.petsmart.util.DAOFactory;

public class RacaRN {

	private RacaDAO racaDAO;

	public RacaRN() {
		this.racaDAO = DAOFactory.criarRacaDAO();
	}

	public Raca carregar(Integer id) {
		return this.racaDAO.carregar(id);
	}

	public void salvar(Raca raca) {
		this.racaDAO.salvar(raca);
	}

	public void atualizar(Raca raca) {
         this.racaDAO.atualizar(raca);
	}

	public void excluir(Raca raca) {
		this.racaDAO.excluir(raca);
	}

	public List<Raca> listar() {
		return this.racaDAO.listar();
	}
	
	

}
