package br.com.esii.petsmart.util;

public class DAOExcpetion  extends Exception{
	
	public DAOExcpetion(){}
	
	public DAOExcpetion(String message){
		super(message);
	}
	
	public DAOExcpetion(Throwable cause){
		super(cause);
	}
	
	public DAOExcpetion(String message, Throwable cause){
		super(message,cause);
	}
	
	public DAOExcpetion(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace){
		super(message,cause,enableSuppression,writableStackTrace);
	}

}
