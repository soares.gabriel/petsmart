package br.com.esii.petsmart.util;

import br.com.esii.petsmart.animal.AnimalDAO;
import br.com.esii.petsmart.animal.AnimalDAOHibernate;
import br.com.esii.petsmart.cargo.CargoDAO;
import br.com.esii.petsmart.cargo.CargoDAOHibernate;
import br.com.esii.petsmart.cliente.ClienteDAO;
import br.com.esii.petsmart.cliente.ClienteDAOHibernate;
import br.com.esii.petsmart.consulta.ConsultaDAO;
import br.com.esii.petsmart.consulta.ConsultaDAOHibernate;
import br.com.esii.petsmart.endereco.EnderecoDAO;
import br.com.esii.petsmart.endereco.EnderecoDAOHibernate;
import br.com.esii.petsmart.especie.EspecieDAO;
import br.com.esii.petsmart.especie.EspecieDAOHibernate;
import br.com.esii.petsmart.estoque.EstoqueDAO;
import br.com.esii.petsmart.estoque.EstoqueDAOHibernate;
import br.com.esii.petsmart.exame.ExameDAO;
import br.com.esii.petsmart.exame.ExameDAOHibernate;
import br.com.esii.petsmart.fornecedor.FornecedorDAO;
import br.com.esii.petsmart.fornecedor.FornecedorDAOHibernate;
import br.com.esii.petsmart.funcionario.FuncionarioDAO;
import br.com.esii.petsmart.funcionario.FuncionarioDAOHibernate;
import br.com.esii.petsmart.itemProduto.ItemProdutoDAO;
import br.com.esii.petsmart.itemProduto.ItemProdutoDAOHibernate;
import br.com.esii.petsmart.produto.ProdutoDAO;
import br.com.esii.petsmart.produto.ProdutoDAOHibernate;
import br.com.esii.petsmart.raca.RacaDAO;
import br.com.esii.petsmart.raca.RacaDAOHibernate;
import br.com.esii.petsmart.veterinario.VeterinarioDAO;
import br.com.esii.petsmart.veterinario.VeterinarioDAOHibernate;


public class DAOFactory {
	
	public static VeterinarioDAO criarVeterinarioDAO(){
		VeterinarioDAOHibernate veterinarioDAO =  new VeterinarioDAOHibernate();
		veterinarioDAO.setSession(HibernateUtil.getSessionfactory().getCurrentSession());
		return veterinarioDAO;
	}
		
	public static ConsultaDAO criarConsultaDAO(){
		ConsultaDAOHibernate consultaDAO =  new ConsultaDAOHibernate();
		consultaDAO.setSession(HibernateUtil.getSessionfactory().getCurrentSession());
		return consultaDAO;
	}
	
	public static ExameDAO criarExameDAO(){
		ExameDAOHibernate exameDAO =  new ExameDAOHibernate();
		exameDAO.setSession(HibernateUtil.getSessionfactory().getCurrentSession());
		return exameDAO;
	}
	
	public static FuncionarioDAO criarFuncionarioDAO(){
		FuncionarioDAOHibernate funcionarioDAO =  new FuncionarioDAOHibernate();
		funcionarioDAO.setSession(HibernateUtil.getSessionfactory().getCurrentSession());
		return funcionarioDAO;
	}
	
	public static CargoDAO criarCargoDAO(){
		CargoDAOHibernate cargoDAO =  new CargoDAOHibernate();
		cargoDAO.setSession(HibernateUtil.getSessionfactory().getCurrentSession());
		return cargoDAO;
	}
	
	public static EstoqueDAO criarEstoqueDAO(){
		EstoqueDAOHibernate estoqueDAO =  new EstoqueDAOHibernate();
		estoqueDAO.setSession(HibernateUtil.getSessionfactory().getCurrentSession());
		return estoqueDAO;
	}
	
	public static EspecieDAO criarEspecieDAO(){
		EspecieDAOHibernate especieDAO =  new EspecieDAOHibernate();
		especieDAO.setSession(HibernateUtil.getSessionfactory().getCurrentSession());
		return especieDAO;
	}
	
	public static EnderecoDAO criarEnderecoDAO(){
		EnderecoDAOHibernate enderecoDAO =  new EnderecoDAOHibernate();
		enderecoDAO.setSession(HibernateUtil.getSessionfactory().getCurrentSession());
		return enderecoDAO;
	}

	public static ClienteDAO criarClienteDAO(){
		ClienteDAOHibernate clienteDAO =  new ClienteDAOHibernate();
		clienteDAO.setSession(HibernateUtil.getSessionfactory().getCurrentSession());
		return clienteDAO;
	}
	
	public static RacaDAO criarRacaDAO(){
		RacaDAOHibernate racaDAO =  new RacaDAOHibernate();
		racaDAO.setSession(HibernateUtil.getSessionfactory().getCurrentSession());
		return racaDAO;
	}

	public static AnimalDAO criarAnimalDAO(){
		AnimalDAOHibernate animalDAO =  new AnimalDAOHibernate();
		animalDAO.setSession(HibernateUtil.getSessionfactory().getCurrentSession());
		return animalDAO;
	}
	
	public static ProdutoDAO criarProdutoDAO(){
		ProdutoDAOHibernate produtoDAO =  new ProdutoDAOHibernate();
		produtoDAO.setSession(HibernateUtil.getSessionfactory().getCurrentSession());
		return produtoDAO;
	}
	
	public static ItemProdutoDAO criarItemProdutoDAO(){
		ItemProdutoDAOHibernate itemProdutoDAO =  new ItemProdutoDAOHibernate();
		itemProdutoDAO.setSession(HibernateUtil.getSessionfactory().getCurrentSession());
		return itemProdutoDAO;
	}
	
	public static FornecedorDAO criarFornecedorDAO(){
		FornecedorDAOHibernate fornecedorDAO =  new FornecedorDAOHibernate();
		fornecedorDAO.setSession(HibernateUtil.getSessionfactory().getCurrentSession());
		return fornecedorDAO;
	}
	

}
