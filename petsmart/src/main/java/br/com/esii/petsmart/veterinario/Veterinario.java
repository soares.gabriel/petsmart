package br.com.esii.petsmart.veterinario;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Embedded;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import br.com.esii.petsmart.agenda.Agenda;
import br.com.esii.petsmart.entidades.EntidadeAbst;
import br.com.esii.petsmart.funcionario.Funcionario;

@Entity
@Table(name="veterinario")
public class Veterinario extends EntidadeAbst implements Serializable{
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 3764970116309987412L;

	@GeneratedValue
	@Id
	@Column(name="veterinario_id")
	private Integer id;
		
	@Column(unique = true)
	private String crmv;
	
	@OneToOne(cascade = CascadeType.ALL)
	private Agenda agendaAtendimento;
	
	@OneToOne
	@JoinColumn(name="funcionario_id")
	private Funcionario funcionario;
	
	public Veterinario(){
		super();
	}

	public String getCrmv() {
		return crmv;
	}

	public void setCrmv(String crmv) {
		this.crmv = crmv;
	}

	public Agenda getAgendaAtendimento() {
		return agendaAtendimento;
	}

	public void setAgendaAtendimento(Agenda agendaAtendimento) {
		this.agendaAtendimento = agendaAtendimento;
	}

	public Funcionario getFuncionario() {
		return funcionario;
	}

	public void setFuncionario(Funcionario funcionario) {
		this.funcionario = funcionario;
	}

	
}
