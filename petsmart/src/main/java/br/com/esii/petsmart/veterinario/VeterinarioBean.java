package br.com.esii.petsmart.veterinario;

import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import br.com.esii.petsmart.funcionario.Funcionario;

public class VeterinarioBean {
	
	private Veterinario veterinario;
	private Veterinario veterinarioSelecionado;
	
	public VeterinarioBean(){
		this.veterinario = new Veterinario();
		this.veterinarioSelecionado = new Veterinario();
	}

	public Veterinario getVeterinario() {
		return veterinario;
	}

	public void setVeterinario(Veterinario veterinario) {
		this.veterinario = veterinario;
	}

	public Veterinario getVeterinarioSelecionado() {
		return veterinarioSelecionado;
	}

	public void setVeterinarioSelecionado(Veterinario veterinarioSelecionado) {
		this.veterinarioSelecionado = veterinarioSelecionado;
	}
	
	public String salvar(){
		VeterinarioRN veterinarioRN = new VeterinarioRN();
		veterinarioRN.salvar(veterinario);
        this.veterinario = new Veterinario();
		FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "", "Veterinario cadastrado com sucesso."));
		return "/restrito/administracao/veterinario/index";
	}
	
	public List<Veterinario> listarTodos(){
	   VeterinarioRN veterinarioRN = new VeterinarioRN();
       return veterinarioRN.listar();
	}
	
	public String deletar(){
		VeterinarioRN veterinarioRN = new VeterinarioRN();
		veterinarioRN.excluir(veterinarioSelecionado);
		FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "", "Veterinario deletado com sucesso."));
		return "/restrito/administracao/veterinario/index";
	}
	
	public String alterar(){
		VeterinarioRN veterinarioRN = new VeterinarioRN();
		veterinarioRN.atualizar(veterinarioSelecionado);
		FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "", "Veterinario alterado com sucesso."));
		return "/restrito/administracao/veterinario/index";
	}

}
