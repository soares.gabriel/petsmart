package br.com.esii.petsmart.veterinario;

import java.util.List;


public interface VeterinarioDAO {
	
	public void salvar(Veterinario veterinario);
	public void atualizar(Veterinario veterinario);
	public void excluir(Veterinario veterinario);
	public Veterinario carregar(Integer id);
	public List<Veterinario> listar();

}
