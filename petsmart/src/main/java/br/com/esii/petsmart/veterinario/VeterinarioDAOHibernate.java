package br.com.esii.petsmart.veterinario;

import java.util.List;

import org.hibernate.Session;

public class VeterinarioDAOHibernate implements VeterinarioDAO{
	
private Session session;
	
	public void setSession(Session session){
		this.session = session;
	}

	@Override
	public void salvar(Veterinario veterinario) {
		this.session.save(veterinario);
	}

	@Override
	public void atualizar(Veterinario veterinario) {
		this.session.update(veterinario);;	
	}

	@Override
	public void excluir(Veterinario veterinario) {
		this.session.delete(veterinario);	
	}

	@Override
	public Veterinario carregar(Integer id) {
		return (Veterinario) this.session.get(Veterinario.class, id);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Veterinario> listar() {
		return this.session.createCriteria(Veterinario.class).list();
	}

}
