package br.com.esii.petsmart.veterinario;

import java.util.List;

import br.com.esii.petsmart.util.DAOFactory;

public class VeterinarioRN {
	
	private VeterinarioDAO veterinarioDAO;
	
	public VeterinarioRN(){
		this.veterinarioDAO = DAOFactory.criarVeterinarioDAO();
	}

	public Veterinario carregar(Integer id){
		return this.veterinarioDAO.carregar(id);
	}
	
	public void salvar(Veterinario veterinario){
		this.veterinarioDAO.salvar(veterinario);
	
	}
	
	public void atualizar(Veterinario veterinario){
		this.veterinarioDAO.atualizar(veterinario);
	}
	
	public void excluir(Veterinario fornecedor){
		this.veterinarioDAO.excluir(fornecedor);
	}
	
	public List<Veterinario> listar(){
		return this.veterinarioDAO.listar();
	}

}
