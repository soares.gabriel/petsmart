function handleRegistrationRequest(xhr, status, args) {
	if (args.validationFailed || !args.registered) {
		$("#CadastroPetId").effect("shake", {times : 3}, 100);
	} else {
		CadastroPet.hide();
		$("#createButton").fadeOut();
	}
}

function fixPFMDialogs() {
	jQuery("body > div[data-role='page']").each(
			function(i) {
				var pageId = jQuery(this).attr("id");
				jQuery("body > div[id*='" + pageId + "'][class*='ui-popup']")
						.appendTo("#" + pageId);
			});
}